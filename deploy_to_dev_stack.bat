@echo off
Rem This script will automatically deploy the DEV stack for the druj solution.
Rem Params:
Rem %1: Version (string) : The major, minor and subminor version of this deployment.
Rem Execute usage: deploy_to_dev_stack.bat 0.0.1
Rem Refer to GitLab env vars on CI config for current version.

rem Restore, Clean, Build!
echo Building..
dotnet restore
dotnet clean
dotnet build --configuration Release
dotnet publish --configuration Release

echo Deploying..
cmd /C call deploy/deploy.bat %1
echo Deployment complete.

echo Script completed. Last updated: %time%
pause