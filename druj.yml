AWSTemplateFormatVersion: "2010-09-09"
Transform: AWS::Serverless-2016-10-31
Description: Fetches weather data from OpenWeatherMap and delivers to Discord via webhook.

Parameters:
    # Deployment environment
    DeployStage:
        Type: String
        Default: dev
        AllowedValues:
            - dev
            - release

    # Solution version, to be supplied in format: Release.Major.Minor E.G 0.0.1
    Version:
        Type: String

    # Webhook URL for lambda use when delivering notifications
    WebhookUrl:
        Type: String

    # API key for the OpenWeather API
    ApiKey:
        Type: String

Resources:

    # Lambda components

    # Lambda function
    Lambda:
        Type: 'AWS::Serverless::Function'
        Properties:
            FunctionName: !Sub '${DeployStage}_druj'
            CodeUri: druj/bin/Release/netcoreapp3.1/publish
            Handler: druj::druj.Druj::Main
            Runtime: dotnetcore3.1
            Description: Fetches weather data from OpenWeatherMap and delivers to Discord via webhook.
            MemorySize: 128
            Timeout: 30
            DeadLetterQueue:
                TargetArn: 
                  !Sub 'arn:aws:sns:eu-west-1:402235557510:${DeployStage}_notipy_submission'
                Type: SNS
            Role: !GetAtt LambdaRole.Arn
            Environment:
                Variables:
                    API_KEY: !Ref ApiKey
                    DEPLOY_STAGE: !Ref DeployStage
                    NOTIFY_TOPIC_ARN: !Sub 'arn:aws:sns:eu-west-1:402235557510:${DeployStage}_notipy_submission'
                    DISPLAY_AVATAR_URL: https://s3.eu-west-1.amazonaws.com/blizzardsev.store.public/pfp/druj.png
                    DISPLAY_USERNAME: Druj
                    TAGGED_USER_IDS: '321054716748365825'
                    WEBHOOK_URL: !Ref WebhookUrl
            Events:
                Schedule:
                    Type: Schedule
                    Properties: 
                        # Trigger daily
                        Schedule: 'rate(3 days)'
                        Name: !Sub '${DeployStage}_druj_schedule'
                        Description: !Sub 'Schedule for the ${DeployStage}_druj solution.'
            Tags:
                STACK: DRUJ
                VERSION: !Ref Version
                INPUT_FROM: CW_SCHEDULE
                OUTPUT_TO: DISCORD

    # Lambda IAM role
    LambdaRole:
        Type: AWS::IAM::Role
        Properties: 
            AssumeRolePolicyDocument: {
                "Version": "2012-10-17",
                "Statement": [
                  {
                      # Allow lambda service to assume the role
                      "Effect": "Allow",
                      "Principal": {
                          "Service": "lambda.amazonaws.com"
                      },
                      "Action": "sts:AssumeRole"
                  }
                ]
              }
            Policies:
                - PolicyName: !Sub '${DeployStage}_druj_policy'
                  PolicyDocument: {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            # Basic permissions for Lambda execution
                            "Sid": "ExecutionPerms",
                            "Effect": "Allow",
                            "Action": [
                                "logs:CreateLogGroup",
                                "logs:CreateLogStream",
                                "logs:PutLogEvents",
                                "ec2:CreateNetworkInterface",
                                "ec2:DeleteNetworkInterface",
                                "ec2:DescribeNetworkInterfaces"
                            ],
                            "Resource": "*"
                        },
                        {
                            # SNS permissions for delivery of notifications to notification topic
                            "Sid": "AlertTopicPerms",
                            "Effect": "Allow",
                            "Action": [
                                "SNS:Subscribe",
                                "SNS:Publish"
                            ],
                            "Resource": {
                                "Fn::Sub": [
                                    "arn:aws:sns:eu-west-1:402235557510:${DeployStage}_notipy_submission",
                                    { "DeployStage": { "Ref" : "DeployStage" }  } 
                                ]
                            }
                        }
                    ]
                }
            Description: Role for the druj lambda.
            RoleName: !Sub '${DeployStage}_druj_role'

    # Monitoring components

    # Alarm for Lambda hard failures, where exceptions are thrown
    LambdaExceptionAlarm:
        Type: AWS::CloudWatch::Alarm
        Properties:
            AlarmDescription: Alarm to monitor unhandled exceptions for the druj lambda.
            AlarmName: !Sub '${DeployStage}_druj_alarm'
            AlarmActions:
                - !Sub 'arn:aws:sns:eu-west-1:402235557510:${DeployStage}_notipy_submission'
            ComparisonOperator: GreaterThanOrEqualToThreshold
            Dimensions:
                - Name: FunctionName
                  Value: !Ref Lambda
            DatapointsToAlarm: 1
            EvaluationPeriods: 1
            MetricName: Errors
            Namespace: AWS/Lambda
            Period: 600
            Statistic: Sum
            Threshold: 1
            Unit: Count