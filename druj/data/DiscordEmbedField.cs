﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace druj.data
{
	/// <summary>
	/// Models field information that an embed may contain.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class DiscordEmbedField
	{
		/// <summary>
		/// Gets the Name; the name displayed for this field on the embed.
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Gets the Value; the value displayed corresponding to the name on the embed.
		/// </summary>
		public string Value { get; private set; }

		/// <summary>
		/// Gets the Inline state; whether this field should be displayed inline or on a new line on the embed.
		/// </summary>
		public bool Inline { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscordEmbedField"/> class.
		/// </summary>
		/// <param name="name">The name to set.</param>
		/// <param name="value">The value to set.</param>
		/// <param name="inline">The inline state to set.</param>
		public DiscordEmbedField(string name, string value, bool inline)
		{
			Name = name;
			Value = value;
			Inline = inline;
		}
	}
}