﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace druj.data
{
	/// <summary>
	/// Models the footer information that an embed may contain.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class DiscordEmbedFooter
	{
		/// <summary>
		/// Gets the Text; the information to display on the footer segment of an embed.
		/// </summary>
		public string Text { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscordEmbedFooter"/> class.
		/// </summary>
		/// <param name="text">The text to set.</param>
		public DiscordEmbedFooter(string text)
		{
			Text = text;
		}
	}
}