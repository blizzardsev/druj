﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace druj.data
{
    /// <summary>
    /// Models the embed information fed to the Discord webhook.
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DiscordEmbed
    {
        /// <summary>
        /// Gets the Title; the title of this embed to display.
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Gets the Description; the description of this embed to display.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the Colour; the colour used for the highlight on the embed.
        /// </summary>
        [JsonProperty("color")]
        public int Colour { get; private set; }

        /// <summary>
        /// Gets the Fields; the individual rows of data this embed will contain.
        /// </summary>
        public List<DiscordEmbedField> Fields { get; private set; }

        /// <summary>
        /// Gets the Footer; the footer text for this embed.
        /// </summary>
        public DiscordEmbedFooter Footer { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscordEmbed"/> class.
        /// </summary>
        /// <param name="title">The title to set.</param>
        /// <param name="description">The description to set.</param>
        /// <param name="colour">The colour to set.</param>
        /// <param name="fields">The fields to set.</param>
        /// <param name="footer">The footer to set.</param>
        public DiscordEmbed(
            string title,
            string description,
            int colour,
            List<DiscordEmbedField> fields,
            DiscordEmbedFooter footer)
		{
            Title = title;
            Description = description;
            Colour = colour;
            Fields = fields;
            Footer = footer;
		}

        /// <summary>
        /// Adds a new field to the list of existing fields on this embed.
        /// </summary>
        /// <param name="field">The field to add.</param>
        public void AddField(DiscordEmbedField field)
		{
            Fields.Add(field);
		}
    }
}