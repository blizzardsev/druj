﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace druj.data
{
	/// <summary>
	/// Models the daily data returned from the OpenWeatherMap API.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class DailyData
	{
		/// <summary>
		/// Gets the UnixDateTime; unix timestamp representing the date and time this data corresponds to.
		/// </summary>
		[JsonProperty("dt")]
		public int UnixDateTime { get; set; }

		/// <summary>
		/// Gets the Humidity; the humidity level for this day. 
		/// </summary>
		public decimal Humidity { get; set; }

		/// <summary>
		/// Gets the Temparature; object containing the temparature data for a given day.
		/// </summary>
		[JsonProperty("temp")]
		public TemparatureData Temparature { get; set; }

		/// <summary>
		/// Gets the Weather; object containing the weather condition data for a given day.
		/// </summary>
		public List<WeatherData> Weather { get; set; }
	}
}