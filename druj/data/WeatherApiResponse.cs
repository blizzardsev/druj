﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace druj.data
{
	/// <summary>
	/// Models the overarching data format returned from the OpenWeatherMap API.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class WeatherApiResponse
	{
		/// <summary>
		/// Gets the Data; collection of data packets describing the weather coming up over several days.
		/// </summary>
		[JsonProperty("daily")]
		public List<DailyData> Data { get; private set; }
	}
}