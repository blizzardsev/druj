﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace druj.data
{
	/// <summary>
	/// Models the temparature data returned from the OpenWeatherMap API.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class TemparatureData
	{
		/// <summary>
		/// Gets the MinimumTemparature; the lowest temperature for a given day.
		/// </summary>
		[JsonProperty("min")]
		public decimal MinimumTemparature { get; private set; }

		/// <summary>
		/// Gets the MaximumTemparature; the highest temperature for a given day.
		/// </summary>
		[JsonProperty("max")]
		public decimal MaximumTemparature { get; private set; }
	}
}