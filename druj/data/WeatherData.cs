﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace druj.data
{
	/// <summary>
	/// Models the weather data returned from the OpenWeatherMap API.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
	public class WeatherData
	{
		/// <summary>
		/// Gets the description; generally describes the conditions.
		/// </summary>
		public string Description { get; set; }
	}
}