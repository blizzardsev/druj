﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace druj.data
{
	/// <summary>
	/// Models the payload sent to a Discord webhook in order to post a message.
	/// </summary>
	[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DiscordWebhookPayload
    {
        /// <summary>
        /// Gets the Username; the name to display as the sender of the message.
        /// </summary>
        public string Username { get; private set; }

        /// <summary>
        /// Gets the AvatarUrl; the profile picture to display alongside the username of the sender.
        /// </summary>
        [JsonProperty("avatar_url")]
        public string AvatarUrl { get; private set; }

        /// <summary>
        /// Gets the Embeds; the list of data segments in the message.
        /// </summary>
        public List<DiscordEmbed> Embeds { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscordWebhookPayload"/> class.
        /// </summary>
        /// <param name="username">The username to set.</param>
        /// <param name="avatarUrl">The avatar URL to set.</param>
        /// <param name="embeds">The embed list to set.</param>
        public DiscordWebhookPayload(string username, string avatarUrl, List<DiscordEmbed> embeds)
        {
            Username = username;
            AvatarUrl = avatarUrl;
            Embeds = embeds;
        }

        /// <summary>
        /// Adds a new embed to the list of existing embeds for this payload.
        /// </summary>
        /// <param name="embed">The new embed to add.</param>
        public void AddEmbed(DiscordEmbed embed)
        {
            Embeds.Add(embed);
        }
    }
}