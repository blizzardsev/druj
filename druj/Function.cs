using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using Amazon.Lambda.Core;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using druj.data;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace druj
{
    /// <summary>
    /// The purpose of Druj is to fetch weather information from the OpenWeatherMap API and deliver it to Discord via webhook.
    /// </summary>
    public class Druj
    {
        /// <summary>
        /// Gets the API key used to authenticate against OpenWeatherMap API.
        /// </summary>
        private string ApiKey { get; }

        /// <summary>
        /// Gets the Discord webhook to deliver messages to.
        /// </summary>
        private string WebhookUrl { get; }
    
        /// <summary>
        /// Default constructor; used to instantiate when deployed.
        /// Attributes set with environment variables defined in SAM.
        /// </summary>
        public Druj()
		{
            ApiKey = Environment.GetEnvironmentVariable("API_KEY");
            WebhookUrl = Environment.GetEnvironmentVariable("WEBHOOK_URL");
        }

        /// <summary>
        /// Manual constructor; used for local instantiation I.E unit tests.
        /// </summary>
        /// <param name="apiKey">The API key to use to authenticate with the OpenWeatherMap API.</param>
        /// <param name="webhookUrl">The webhook URL to post weather updates to.</param>
        public Druj(string apiKey, string webhookUrl)
		{
            ApiKey = apiKey;
            WebhookUrl = webhookUrl;
        }

        /// <summary>
        /// Lambda function entry point.
        /// On a schedule, check the OpenWeatherMap API, grab the weather and ping Discord webhook with update.
        /// </summary>
        public void Main()
        {
            WeatherApiResponse weatherApiData = null;

            // Get the weather data
            try
			{
                weatherApiData = GetWeatherData();
            }
            catch (Exception exception)
			{
                PublishErrorNotification("could not retrieve/serialize weather data from API.", exception);
                return;
			}

            // Send to Discord
            try
			{
                var discordWebhookPayload = new DiscordWebhookPayload(
                    Environment.GetEnvironmentVariable("DISPLAY_USERNAME"),
                    Environment.GetEnvironmentVariable("DISPLAY_AVATAR_URL"),
                    new List<DiscordEmbed>() 
                    { 
                        new DiscordEmbed(
                            title:"Weather Report!",
                            description:"Here's the weather coming up for the next few days:",
                            colour:14414552,
                            fields:new List<DiscordEmbedField>(),
                            footer:new DiscordEmbedFooter($"Delivered: {DateTime.Now:dd/MM/yyyy, HH:mm}"))
                    });

                // Add a field for each daily weather data packet
                var textInfo = new CultureInfo("en-GB", false).TextInfo;
                var dayCount = 0;
                foreach (var weatherData in weatherApiData.Data)
				{
                    var weatherSummary = 
                        $"{textInfo.ToTitleCase(weatherData.Weather[0].Description)} " +
                        $"| Temperature: {weatherData.Temparature.MinimumTemparature} : {weatherData.Temparature.MaximumTemparature} {Convert.ToChar(176)}C " +
                        $"| Humidity: {weatherData.Humidity}";

                    discordWebhookPayload.Embeds[0].AddField(
                        new DiscordEmbedField(
                            name:DateTimeOffset.FromUnixTimeSeconds(weatherData.UnixDateTime).UtcDateTime.ToString("ddd, dd/MM/yyyy"),
                            value:weatherSummary,
                            inline:true));

                    dayCount++;
                    if (dayCount == 3)
					{
                        break;
					}
				}

                // Finally add the tags
                var tags = "";
                foreach (string tag in Environment.GetEnvironmentVariable("TAGGED_USER_IDS").Split(','))
				{
                    tags += $"<@{tag}>";
				}
                discordWebhookPayload.Embeds[0].AddField(
                new DiscordEmbedField(
                    name: "Tags",
                    value: tags,
                    inline: false));

                PostToWebhook(discordWebhookPayload);
			}
            catch (Exception exception)
			{
                PublishErrorNotification("could not post weather data to webhook.", exception);
                return;
            }
        }

        /// <summary>
        /// Fetches the weather data for the upcoming week from the OpenWeatherMap API.
        /// </summary>
        /// <returns>Data from the OpenWeatherMap API.</returns>
        public WeatherApiResponse GetWeatherData()
		{
            var apiResponse = new HttpClient()
                .SendAsync(new HttpRequestMessage()
                {
                    RequestUri = new Uri(
                        $"https://api.openweathermap.org/data/2.5/onecall?lat=52.573391&lon=-0.248460&units=metric" +
                        $"&exclude=current,minutely,hourly,alerts&appid={ApiKey}")
                }).Result;

            if (apiResponse.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<WeatherApiResponse>(apiResponse.Content.ReadAsStringAsync().Result);
            }
            else
            {
                throw new HttpRequestException($"Http status code was: {apiResponse.StatusCode}");
            }
        }

        /// <summary>
        /// Takes a payload and delivers it to the configured Discord webhook.
        /// </summary>
        public void PostToWebhook(DiscordWebhookPayload discordPayload)
		{
            var apiResponse = new HttpClient().PostAsync(
                new Uri(WebhookUrl),
                new StringContent(
                    content:JsonConvert.SerializeObject(discordPayload),
                    encoding:Encoding.UTF8,
                    mediaType:MediaTypeNames.Application.Json)).Result;

            if (apiResponse.StatusCode == HttpStatusCode.NoContent)
            {
                return;
            }
            else
            {
                throw new HttpRequestException($"Http status code was: {apiResponse.StatusCode}");
            }
        }

        /// <summary>
        /// Posts an error to the notification topic for delivery to Discord alert channel.
        /// </summary>
        /// <param name="errorReason">Concise reason for the error.</param>
        /// <param name="exceptionData">Exception information dump for debugging.</param>
        private async void PublishErrorNotification(string errorReason, Exception exceptionData)
		{
            using var snsClient = new AmazonSimpleNotificationServiceClient();
            await snsClient.PublishAsync(new PublishRequest()
            {
                TopicArn = Environment.GetEnvironmentVariable("NOTIFY_TOPIC_ARN"),
                Subject = $"{Environment.GetEnvironmentVariable("DEPLOY_STAGE").ToLower()}_druj failed: {errorReason}",
                Message = JsonConvert.SerializeObject(exceptionData),
                MessageAttributes = new Dictionary<string, MessageAttributeValue>()
				{
					{ 
                        "DiscordDisplayAllData", new MessageAttributeValue()
					    {
						    DataType = "String",
						    StringValue = "true"
					    }
                    }
				}
			});
        }
    }
}