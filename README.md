# About druj

Druj is a C# .NET Core 3.1 lambda application designed to retrieve weather data from the OpenWeatherMap API to deliver updates to Discord through a specified webhook on a schedule.

An API key from the OpenWeatherMap API and a Discord webhook URL are needed to make use of Druj.

Get an OpenWeatherMap API key @ https://openweathermap.org

Druj is designed to be deployed to AWS via CloudFormation with SAM.

## CI/deployment script configuration

### Runtime

* .NET Core 3.1

### Dependencies

* Amazon.Lambda.Core
* Amazon.Lambda.Serialization
* AWSSDK.SimpleNotificationService
* Newtonsoft.Json

### Environment variables
* **API_KEY** : API key for the OpenWeatherMap API.
* **DEPLOY_STAGE** : 
* **NOTIFY_TOPIC_ARN** : The ARN of the alert topic to send notifications to in the event of controlled failure.
* **DISPLAY_AVATAR_URL** : The URL specifying the profile picture to display for the sender of the messages on Discord.
* **DISPLAY_USERNAME** : The username to display as the sender of the messages on Discord.
* **TAGGED_USER_IDS** : The IDs of the users to tag on Discord. Each ID must be comma-separated.
* **WEBHOOK_URL** : The URL identifying the channel on Discord to post to.

## Deployment

Use script *deploy_to_dev_stack.bat* *version-number* *webhook-url* *api-key* for manual deployments; E.G `deploy_to_dev_stack.bat 1.0.0 my-webhook-url my-api-key`. Execute within Druj directory.
Release deployments are handled by CI.

## Contact

*blizzardsev.dev@gmail.com*