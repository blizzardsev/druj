using System.Collections.Generic;
using System;
using Xunit;
using druj.data;

namespace druj.Tests
{
    /// <summary>
    /// Examines functionality of the Druj lambda.
    /// </summary>
    public class DrujTest
    {
        /// <summary>
        /// Gets the Druj lambda; instance that will be tested.
        /// </summary>
        private Druj Druj { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrujTest"/> class.
        /// Instantiates the Druj property, assigns values from resources ready for tests.
        /// </summary>
        public DrujTest()
		{
            Druj = new Druj(apiKey:Properties.Resources.ApiKey, webhookUrl:Properties.Resources.WebhookUrl);
		}

        /// <summary>
        /// Test to verify that the GetWeatherData function queries the OpenWeatherMap API correctly and returns a valid response.
        /// </summary>
        [Fact]
        private void GetWeatherData_ReturnsWeatherData()
		{
            var weatherApiResponse = Druj.GetWeatherData();

			// Test the master object
			Assert.NotNull(weatherApiResponse);
            Assert.IsType<WeatherApiResponse>(weatherApiResponse);
            Assert.NotEmpty(weatherApiResponse.Data);

            // Test each property
            foreach (var data in weatherApiResponse.Data)
			{
                Assert.NotEqual(0, data.UnixDateTime);
                Assert.NotEqual(0, data.Humidity);
				Assert.NotNull(data.Temparature);
                Assert.NotEqual(0, data.Temparature.MinimumTemparature);
                Assert.NotEqual(0, data.Temparature.MaximumTemparature);
                Assert.NotEmpty(data.Weather);
                Assert.NotNull(data.Weather[0].Description);
            }
        }

        /// <summary>
        /// Test to verify that the PostToWebhook function posts to the webhook correctly and returns a valid response.
        /// </summary>
        [Fact]
        private void PostToWebhook_PostsToWebhook()
		{
            // Build the main payload
            var discordWebhookPayload = new DiscordWebhookPayload(
                    Properties.Resources.DisplayUsername,
                    Properties.Resources.DisplayAvatarUrl,
                    new List<DiscordEmbed>()
                    {
                        new DiscordEmbed(
                            title:"Test Weather Report!",
                            description:"Here's the weather coming up for the next few days:",
                            colour:14414552,
                            fields:new List<DiscordEmbedField>(),
                            footer:new DiscordEmbedFooter($"Delivered: {DateTime.Now:dd/MM/yyyy, HH:mm}"))
                    });

            // Construct the weather summary, as this gets complex
            var weatherSummary =
                $"Debug Weather " +
                $"| Temperature: 0 - 30 {Convert.ToChar(176)}C " +
                $"| Humidity: 5";

            // Add the fields
            discordWebhookPayload.Embeds[0].AddField(
                new DiscordEmbedField(
                    name: DateTimeOffset.FromUnixTimeSeconds(DateTimeOffset.Now.ToUnixTimeSeconds()).UtcDateTime.ToString("ddd, dd/MM/yyyy"),
                    value: weatherSummary,
                    inline: true));

            discordWebhookPayload.Embeds[0].AddField(
                new DiscordEmbedField(
                    name: "Tags",
                    value: $"<@{Properties.Resources.TaggedUserId}>",
                    inline: false));

            // Finally send
            Druj.PostToWebhook(discordWebhookPayload);
        }
    }
}